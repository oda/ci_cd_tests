# ROOTのベースイメージ
FROM rootproject/root-conda:6.20.00
 
# レポジトリの内容をコンテナにコピー
COPY . /tutorial2020
 
# /analysisディレクトリをデフォルト作業ディレクトリに設定
WORKDIR /tutorial2020/
 
# analysis.cxxをコンパイル
RUN echo ">>> Compile analysis executable ..." &&  \
cd analysis && \
COMPILER=$(root-config --cxx) &&  \
FLAGS=$(root-config --cflags --libs) &&  \
$COMPILER -g -std=c++11 -O3 -Wall -Wextra -Wpedantic -o analysis analysis.cxx $FLAGS
